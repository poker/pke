/// <reference path="../typings/chai/chai.d.ts" />
/// <reference path="../typings/mocha/mocha.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import { expect } from 'chai';

import PokerHand from '../src/PokerHand';
import HandStrength from '../src/HandStrength';
import Card from '../src/Card';
import Ranks from '../src/Ranks';
import Suits from '../src/Suits';

describe('Poker hand evaluation', () => {

  var 
    royal: PokerHand, 
    straightFlush: PokerHand, 
    quads: PokerHand, 
    fullHouse: PokerHand, 
    flush: PokerHand, 
    straightNoWheel: PokerHand, 
    straightWheel: PokerHand, 
    set: PokerHand, 
    twoPair: PokerHand, 
    onePair: PokerHand, 
    highCard: PokerHand;

  it('Royal flush', () => {
    royal = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.CLUB),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.QUEEN, Suits.values.CLUB)
    ]);
    expect(royal.getStrength()).to.eq(HandStrength.StraightFlush);
  });

  it('Straight Flush', () => {
    straightFlush = new PokerHand([
      new Card(Ranks.values.NINE, Suits.values.DIAMOND),
      new Card(Ranks.values.KING, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.TEN, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.DIAMOND),
    ]);
    expect(straightFlush.getStrength()).to.eq(HandStrength.StraightFlush);
  });

  it('Four of a kind', () => {
    quads = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.SPADE),
      new Card(Ranks.values.JACK, Suits.values.HEART),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(quads.getStrength()).to.eq(HandStrength.Quads);
  });

  it('Full house', () => {
    fullHouse = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(fullHouse.getStrength()).to.eq(HandStrength.FullHouse);
  });

  it('Flush', () => {
    flush = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.DEUCE, Suits.values.CLUB),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(flush.getStrength()).to.eq(HandStrength.Flush);
  });

  it('Straight', () => {
    straightNoWheel = new PokerHand([
      new Card(Ranks.values.NINE, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.TEN, Suits.values.SPADE),
      new Card(Ranks.values.QUEEN, Suits.values.CLUB)
    ]);
    expect(straightNoWheel.getStrength(), 'no wheel').to.eq(HandStrength.Straight);

    straightWheel = new PokerHand([
      new Card(Ranks.values.FOUR, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.DEUCE, Suits.values.CLUB),
      new Card(Ranks.values.FIVE, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART)
    ]);
    expect(straightWheel.getStrength(), 'wheel').to.eq(HandStrength.Straight);
  });

  it('Three of a kind', () => {
    set = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.ACE, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(set.getStrength()).to.eq(HandStrength.Set);
  });

  it('Two pair', () => {
    twoPair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.QUEEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getStrength()).to.eq(HandStrength.TwoPair);
  });

  it('One pair', () => {
    onePair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.KING, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(onePair.getStrength()).to.eq(HandStrength.OnePair);
  });

  it('High Card', () => {
    highCard = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.FIVE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(highCard.getStrength()).to.eq(HandStrength.HighCard);
  });

  it('Check score between different hand types', () => {
    expect(royal.getScore(), 'royal').to.be.greaterThan(straightFlush.getScore());
    expect(straightFlush.getScore(), 'straight flush').to.be.greaterThan(quads.getScore());
    expect(quads.getScore(), '4 of a kind').to.be.greaterThan(fullHouse.getScore());
    expect(fullHouse.getScore(), 'full house').to.be.greaterThan(flush.getScore());
    expect(flush.getScore(), 'flush').to.be.greaterThan(straightNoWheel.getScore());
    expect(straightNoWheel.getScore(), 'straight no wheel').to.be.greaterThan(straightWheel.getScore());
    expect(straightWheel.getScore(), 'straight wheel').to.be.greaterThan(set.getScore());
    expect(set.getScore(), '3 of a kind').to.be.greaterThan(twoPair.getScore());
    expect(twoPair.getScore(), '2 pair').to.be.greaterThan(onePair.getScore());
    expect(onePair.getScore(), '1 pair').to.be.greaterThan(highCard.getScore());
  });

  it('Check score of Straight', () => {
    var lowStraight = new PokerHand([
      new Card(Ranks.values.NINE, Suits.values.CLUB),
      new Card(Ranks.values.EIGHT, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.TEN, Suits.values.SPADE),
      new Card(Ranks.values.QUEEN, Suits.values.CLUB)
    ]);
    expect(straightNoWheel.getScore(), 'straight').to.be.greaterThan(lowStraight.getScore());
  });

  it('Check score of Full House', () => {
    var lowFullHouse = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.THREE, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(fullHouse.getScore(), 'full house').to.be.greaterThan(lowFullHouse.getScore());
  });

  it('Check score of Full House', () => {
    var lowFullHouse = new PokerHand([
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.THREE, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(fullHouse.getScore(), 'full house').to.be.greaterThan(lowFullHouse.getScore());
  });

  it('Check score of Two Pair', () => {
    var lowTwoPair = new PokerHand([
      new Card(Ranks.values.KING, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.DIAMOND),
      new Card(Ranks.values.DEUCE, Suits.values.HEART),
      new Card(Ranks.values.DEUCE, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowTwoPair.getScore());

    lowTwoPair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.DEUCE, Suits.values.HEART),
      new Card(Ranks.values.DEUCE, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowTwoPair.getScore());

    lowTwoPair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.QUEEN, Suits.values.CLUB),
      new Card(Ranks.values.DEUCE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowTwoPair.getScore());
  });

  it('Check score of One Pair', () => {
    var lowOnePair = new PokerHand([
      new Card(Ranks.values.KING, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowOnePair.getScore());

    lowOnePair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowOnePair.getScore());

    lowOnePair = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.ACE, Suits.values.DIAMOND),
      new Card(Ranks.values.KING, Suits.values.HEART),
      new Card(Ranks.values.NINE, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowOnePair.getScore());
  });

  it('Check score of High Card', () => {
    var lowHighCard = new PokerHand([
      new Card(Ranks.values.KING, Suits.values.CLUB),
      new Card(Ranks.values.FIVE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowHighCard.getScore());

    lowHighCard = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.FIVE, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowHighCard.getScore());

    highCard = new PokerHand([
      new Card(Ranks.values.ACE, Suits.values.CLUB),
      new Card(Ranks.values.FIVE, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.HEART),
      new Card(Ranks.values.NINE, Suits.values.CLUB),
      new Card(Ranks.values.THREE, Suits.values.CLUB)
    ]);
    expect(twoPair.getScore(), 'two pair').to.be.greaterThan(lowHighCard.getScore());
  });

});
