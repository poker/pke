/// <reference path="../typings/chai/chai.d.ts" />
/// <reference path="../typings/mocha/mocha.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

import { expect } from 'chai';

import Hand from '../src/Hand';
import Card from '../src/Card';
import HandStrength from '../src/HandStrength';
import Ranks from '../src/Ranks';
import Suits from '../src/Suits';

describe('Pick best hand from 7 stub', () => {

  it('Full house', () => {
    var hand = new Hand([
      new Card(Ranks.values.SEVEN, Suits.values.CLUB),
      new Card(Ranks.values.SEVEN, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.SEVEN, Suits.values.HEART),
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.HEART)
    ]);
    expect(hand.getBestPokerHand().getStrength()).to.eq(HandStrength.FullHouse);
  });
  
  it('One pair', () => {
    var hand = new Hand([
      new Card(Ranks.values.EIGHT, Suits.values.HEART),
      new Card(Ranks.values.FIVE, Suits.values.HEART),
      new Card(Ranks.values.SIX, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.SPADE),
      new Card(Ranks.values.EIGHT, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.SPADE)
    ]);
    expect(hand.getBestPokerHand().getStrength()).to.eq(HandStrength.OnePair);
  });
});
