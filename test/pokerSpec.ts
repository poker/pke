/// <reference path="../typings/chai/chai.d.ts" />
/// <reference path="../typings/mocha/mocha.d.ts" />
/// <reference path="../typings/sinon/sinon.d.ts" />
/// <reference path="../typings/underscore/underscore.d.ts" />

declare var require;
require('source-map-support').install();

import { expect } from 'chai';
import * as sinon from 'sinon';

import * as _ from 'underscore';

import { default as Poker, BetRoundEnum } from '../src/Poker';
import Player from '../src/Player';
import Hand from '../src/Hand';
import Card from '../src/Card';
import Ranks from '../src/Ranks';
import Suits from '../src/Suits';

describe('Poker', () => {
  var poker: Poker, start, small, big;
  beforeEach(() => {
    start = 500;
    small = 10;
    big = 25;
    poker = new Poker(6, big, small);
    poker.addPlayer(start, 1);
    poker.addPlayer(start, 2);
    poker.addPlayer(start, 4);
    poker.addPlayer(start, 5);
  });

  it('Table should have correct # of seats', () => {
    expect(poker.tableSize).to.eq(6);
  })
  
  it('Should not add player in unavailabe seats', () => {
    const error = 'unavailable seat';
    expect(() => poker.addPlayer(999, 1)).to.throw(error);
    expect(() => poker.addPlayer(999, -1)).to.throw(error);
    expect(() => poker.addPlayer(999, 6)).to.throw(error);
    expect(() => poker.addPlayer(999, 99)).to.throw(error);
  });

  describe('Blind bets', () => {
    it('Start game', () => {
      var onStart = sinon.spy();
      poker.emitter.on('start', onStart);

      poker.start();

      _.each(poker.players, p => {
        expect(p.hand.getCards().length).to.eq(2);
      });
      expect(poker.dealerIndex, 'dealer index').to.eq(1);
      expect(poker.currPlayerIndex, 'current index').to.eq(2);
      expect(poker.currBet, 'current bet').to.eq(0);
      expect(poker.minRaise, 'minimum raise').to.eq(big);

      sinon.assert.calledOnce(onStart);
    });

    it('Small blind bet', () => {
      poker.start();
      poker.bet(small);
      expect(poker.currPlayerIndex, 'current index').to.eq(4);
      expect(poker.currBet, 'current bet').to.eq(small);
      expect(poker.minRaise, 'minimum raise').to.eq(big);
      expect(poker.smallBlindIndex).to.eq(2);
      expect(poker.players[2].chips).to.eq(start - small);
      expect(poker.players[2].contributedChips).to.eq(small);
      expect(poker.players[2].betAmount).to.eq(small);
    });

    it('Big blind bet', () => {
      poker.start();
      poker.bet(small);
      poker.bet(big);
      expect(poker.currPlayerIndex, 'current index').to.eq(5);
      expect(poker.currBet, 'current bet').to.eq(big);
      expect(poker.minRaise, 'minimum raise').to.eq(big);
      expect(poker.bigBlindIndex).to.eq(4);
    });
  });

  describe('Pre-flop', () => {
    beforeEach(() => {
      poker.start();
      poker.bet(small);
      poker.bet(big);
    });

    it('All call then flop', () => {
      var onFlop = sinon.spy();
      poker.emitter.on('flop', onFlop);
      poker.call();
      poker.call();
      poker.call();
      poker.call();
      expect(poker.getBetRound()).to.eq(BetRoundEnum.FLOP);
      expect(poker.currPlayerIndex).to.eq(2);
      expect(poker.currBet).to.eq(0);

      sinon.assert.calledOnce(onFlop);
    });

    it('Double raise 200', () => {
      poker.raise(big);
      poker.call();
      poker.call();
      poker.call();
      expect(poker.getBetRound()).to.eq(BetRoundEnum.FLOP);
    });

    it('All in', () => {
      var onEnd = sinon.spy();
      poker.emitter.on('end', onEnd);
      poker.allIn();
      poker.call();
      poker.call();
      poker.call();

      sinon.assert.calledOnce(onEnd);
    });
  });

  it('Complete hand', () => {
    var 
      onEnd = sinon.spy(),
      onStart = sinon.spy();
    poker.emitter.on('end', onEnd);
    poker.start();
    poker.emitter.on('start', onStart);
    poker.bet(small);
    poker.bet(big);
    _.times(4, (n) => {
      poker.call();
      poker.call();
      poker.call();
      poker.call();
      if (n < 3) {
        expect(poker.getBetRound()).to.eq(BetRoundEnum.FLOP + n);
      } else {
        sinon.assert.calledOnce(onEnd);
        sinon.assert.calledOnce(onStart);
      }
    });
    expect(poker.dealerIndex).to.eq(2);
    expect(poker.getBetRound()).to.eq(BetRoundEnum.PREFLOP);
  });
});
