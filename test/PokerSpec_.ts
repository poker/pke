/// <reference path="../../typings/chai/chai.d.ts" />
/// <reference path="../../typings/chai/chai.d.ts" />
/// <reference path="../../typings/mocha/mocha.d.ts" />
/// <reference path="../../typings/underscore/underscore.d.ts" />

import { expect } from 'chai';
import * as _ from 'underscore';

import Poker from '../poker/Poker';
import Player from '../poker/Player';
import Hand from '../poker/Hand';
import Card from '../poker/Card';
import Ranks from '../poker/Ranks';
import Suits from '../poker/Suits';

describe('Poker', () => {
  var players: Player[], poker: Poker, button: Player, small: Player, big: Player;

  players = _(3).times(() => { return new Player(100); });

  players[0].removeChips(50);

  poker = new Poker(_(players).map(p => p.getChips()), 0, 2);

  function getPlayer(i: number) { return poker.getPlayers()[i]; }
  function getButton() { return getPlayer(0); };
  function getSmall() { return getPlayer(1); };
  function getBig() { return getPlayer(2); };

  describe('Basic actions', () => {
    it('Init', () => {
      poker.newHand();
      expect(getSmall().getBet(), 'small blind bet 1').to.eq(1);
      expect(poker.getCurrPlayer().getBet()).to.eq(0);
      expect(poker.getCurrBet()).to.eq(2);

      expect(poker.getCurrPlayer()).to.eq(getButton());
    });

    it('Raise 8', () => {
      // under-the-gun / button
      expect(poker.getToCallChips(), 'Check to-call chips').to.eq(2);
      poker.raise(8);

      expect(getButton().getBet()).to.eq(10);
      expect(poker.getCurrBet()).to.eq(10);

      expect(getButton().getChips()).to.eq(40);
      expect(poker.getCurrPlayer()).to.eq(getSmall());
    });

    it('Call', () => {
      // small blind
      expect(poker.getToCallChips(), 'Check to-call chips').to.eq(9);
      poker.call();

      expect(getSmall().getBet()).to.eq(10);
      expect(poker.getCurrBet()).to.eq(10);

      expect(getSmall().getChips()).to.eq(90);
      expect(poker.getCurrPlayer()).to.eq(getBig());
    });

    it('All-in', () => {
      // big blind
      expect(poker.getToCallChips(), 'Check to-call chips').to.eq(8);
      poker.allIn();

      expect(poker.getCurrBet()).to.eq(100);

      expect(getBig().getChips()).to.eq(0);

      expect(poker.getCurrPlayer()).to.eq(getButton());
    });

    it('All-in with less-than-bet chips', () => {
      // button
      expect(poker.getToCallChips(), 'Check to-call chips').to.eq(90);

      poker.allIn();
      expect(poker.getCurrBet()).to.eq(100);
      expect(getButton().getChips()).to.eq(0);
    });

    it('Fold', () => {
      // small
      expect(poker.getToCallChips(), 'Check to-call chips').to.eq(90);
      poker.fold();

      expect(getSmall().isFolded()).to.eq(true);
      expect(poker.getActivePlayers().length).to.eq(2);
    });

  });

  describe('Pot split', () => {
    var players: Player[], hand1: Hand, hand2: Hand;

    beforeEach(() => {
      hand1 = new Hand([
        new Card(Ranks.values.ACE, Suits.values.CLUB),
        new Card(Ranks.values.ACE, Suits.values.HEART),
        new Card(Ranks.values.KING, Suits.values.CLUB),
        new Card(Ranks.values.KING, Suits.values.HEART),
        new Card(Ranks.values.QUEEN, Suits.values.CLUB)
      ]);

      hand2 = new Hand([
        new Card(Ranks.values.ACE, Suits.values.CLUB),
        new Card(Ranks.values.ACE, Suits.values.HEART),
        new Card(Ranks.values.KING, Suits.values.CLUB),
        new Card(Ranks.values.JACK, Suits.values.HEART),
        new Card(Ranks.values.QUEEN, Suits.values.CLUB)
      ]);
    });

    it('Split the side pots', () => {
      players = _(3).times(() => new Player());

      players[0].newHand(hand1);
      players[2].newHand(hand1);
      players[1].newHand(hand2);

      players[0].addChips(100); players[0].bet(100);
      players[1].addChips(80); players[1].bet(80);
      players[2].addChips(20); players[2].bet(20);

      const results = Poker.calculatePots(players);
      _(results).each(res => {
        res.player.addChips(res.winningChips);
      });
      expect(players[0].getChips()).to.eq(170);
      expect(players[1].getChips()).to.eq(0);
      expect(players[2].getChips()).to.eq(30);
    });

    it('Split before showdown', () => {
      players = _(2).times(() => new Player());

      players[0].newHand();
      players[1].newHand();

      players[0].addChips(100); players[0].bet(100);
      players[1].addChips(100); players[1].bet(100);
      players[0].fold();
      const results = Poker.calculatePots(players);
      _(results).each(res => {
        res.player.addChips(res.winningChips);
      });
      expect(players[0].getChips()).to.eq(0);
      expect(players[1].getChips()).to.eq(200);
    });
  });

});
