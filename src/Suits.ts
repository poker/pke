/// <reference path="../typings/underscore/underscore.d.ts" />

export default class Suits {

  private short: string;

  constructor(private full: string, private unicode: string) {
    this.short = this.full.charAt(0);
  }

  getFullName() { return this.full; }

  getShortName() { return this.short; }

  getUnicode() { return this.unicode; }

  static values: SuitsDictionary = {
    CLUB: new Suits('Club', '\u2663'),
    DIAMOND: new Suits('Diamond', '\u2666'),
    HEART: new Suits('Heart', '\u2665'),
    SPADE: new Suits('Spade', '\u2660')
  }
}

export interface SuitsDictionary extends _.Dictionary<Suits> {
  CLUB: Suits,
  DIAMOND: Suits,
  HEART: Suits,
  SPADE: Suits
}
