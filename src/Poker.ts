/// <reference path="../typings/node/node.d.ts" />

import * as _ from 'underscore';
import { EventEmitter } from 'events';

import Player from './Player';
import Deck from './Deck';
import Card from './Card';

export enum BetRoundEnum {
  PREFLOP = 2,
  FLOP = 5,
  TURN = 6,
  RIVER = 7 
}

export default class Poker {
  private deck: Deck;
  players: _.Dictionary<Player>;
  
  dealerIndex: number;
  smallBlindIndex: number;
  bigBlindIndex: number;

  currPlayerIndex: number;

  currBet: number;
  minRaise: number;

  emitter = new EventEmitter();

  constructor(public tableSize: number, public blind: number, public smallBlind: number) {
    this.deck = new Deck();
    this.players = {};
  }

  addPlayer(playerChips: number, seatIndex: number) {
    if (seatIndex < 1 || seatIndex >= this.tableSize || this.players[seatIndex]) {
      throw new Error('unavailable seat');
    }
    const player = new Player(playerChips, seatIndex);
    player.fold();
    this.players[seatIndex] = player;
  }

  start() {
    _.each(this.players, p => { 
      p.newHand(); 
    });

    if (this.getAlivePlayers().length < 2) return;

    this.deck.shuffle();

    _.times(2, () => {
      _.each(this.players, p => { 
        p.addCard(this.deck.dealCard()); 
      });
    });

    this.dealerIndex = this.getNextIndex(_.isUndefined(this.dealerIndex) ? -1 : this.dealerIndex);
    this.currPlayerIndex = this.getNextIndex(this.dealerIndex);

    this.smallBlindIndex = this.bigBlindIndex = null;

    this.minRaise = this.blind;
    this.currBet = 0;

    this.emitter.emit('start', _.map(this.players, (player) => {
      return player.hand.getCards();
    }));
  }

  fold() {
    this.getCurrPlayer().fold();
    this.emitter.emit('fold');
    this.bet(0);
  }

  bet(chips: number) {
    if (_.isNull(this.smallBlindIndex)) {
      this.smallBlindIndex = this.currPlayerIndex;
    } else if (_.isNull(this.bigBlindIndex)) {
      this.bigBlindIndex = this.currPlayerIndex;
    }

    const player = this.getCurrPlayer();
    player.contribute(chips);

    // someone bets or raises
    const raiseAmount = player.betAmount - this.currBet;
    if (raiseAmount > 0) {
      this.currBet = player.betAmount;
      if (this.currBet > this.blind) { 
        this.minRaise = raiseAmount;
      }
    }

    // betting done?
    if (this.isBettingDone()) {
      // TODO: it's either deal card, take pot or showdown here
      const aliveCount = this.getAlivePlayers().length;
      if (this.getBetRound() < BetRoundEnum.RIVER && aliveCount > 1) {
        // deal card
        const cards = this.dealCommonCards();
        this.currPlayerIndex = this.smallBlindIndex;
        this.currBet = 0;
        _.each(this.players, (player) => {
          player.betAmount = 0;
        });
        this.emitter.emit(BetRoundEnum[this.getBetRound()].toLowerCase(), cards);
      } else {
        const allInCount = _.filter(this.players, p => p.isAllIn()).length;
        if (aliveCount + allInCount > 1) {
          // deal card
          while (this.getBetRound() !== BetRoundEnum.RIVER) {
            this.dealCommonCards();
          }
        }
        // calculate pots
        const res = Poker.calculatePots((<any>_.toArray(this.players)));
        _(res).each(result => {
          result.player.chips += result.winningChips;
        });
        this.emitter.emit('end', res);
        this.start();
      }
    } else {
      this.currPlayerIndex = this.getNextIndex();
      this.emitter.emit('nextPlayer');
    }
  }

  check() {
    this.emitter.emit('check');
    this.bet(0);
  }

  call() {
    this.emitter.emit('call');
    this.bet(this.currBet - this.getCurrPlayer().betAmount);
  }

  raise(chips: number) {
    this.emitter.emit('raise', chips);
    this.bet(this.currBet - this.getCurrPlayer().betAmount + chips);
  }

  allIn() {
    this.emitter.emit('allIn');
    this.bet(this.getCurrPlayer().chips);
  }

  getBetRound(): BetRoundEnum {
    return this.getCurrPlayer().hand.getCards().length;
  }

  private getNextIndex(currIndex?: number) {
    const recur = (currIndex) => {
      const nextIndex = (currIndex + 1) % this.tableSize;
      const player = this.players[nextIndex];

      if (player && player.isAlive()) {
        return nextIndex;
      } else if (nextIndex === this.currPlayerIndex) {
        return null;
      } else {
        return recur(nextIndex);
      }
    };
    return recur(currIndex || this.currPlayerIndex);
  }

  private isBettingDone() {
    // no bet, all check?
    if (this.currBet === 0 && this.currPlayerIndex === this.dealerIndex) {
      return true;
    }
    // pre-flop or someone bet/raises
    if (this.currBet > 0) {
      // if current player is small blind, wait for big blind to act
      if (this.bigBlindIndex && this.currPlayerIndex === this.smallBlindIndex && this.players[this.bigBlindIndex].isAlive()) {
        return false;
      }
      return !_.any(this.players, (player, index) => {
        if (!player.isAlive()) return false;

        return player.betAmount < this.currBet;
      });
    }
    return false;
  }

  private getCurrPlayer() {
    return this.players[this.currPlayerIndex];
  }

  private dealCommonCards(): Card[] {
    // deal card(s)
    this.deck.dealCard();
    return _(this.getBetRound() === BetRoundEnum.PREFLOP ? 3 : 1).times(() => {
      const card = this.deck.dealCard();
      _.each(this.players, player => player.addCard(card));
      return card;
    });
  }
  
  private getAlivePlayers() {
    return _.filter(this.players, p => p.isAlive());
  }

  static calculatePots(players: Player[]): PotResult[] {

    function calculate(sorted: PotResult[][]): PotResult[] {
      const results: PotResult[] = _(sorted).flatten();

      if (_(results).every(result => result.contributedChips <= 0)) 
        return results;

      const withBetSorted = _.chain(sorted)
        .map(group => _(group).filter(result => result.contributedChips > 0))
        .filter(group => group.length > 0)
        .value();

      const highScoreGroup = withBetSorted[0],
        lowBetResult = highScoreGroup[0],
        bet = lowBetResult.contributedChips;

      const potChips = _(results).reduce((sum, result: PotResult) => {
        return sum + _.min([bet, result.contributedChips]);
      }, 0);

      const updateSorted = _(sorted).map((group: PotResult[], index) => {
        return _(group).map((result): PotResult => {
          const winningChips = _(highScoreGroup).contains(result) ? Math.floor(potChips / highScoreGroup.length) : 0;
          return {
            player: result.player,
            contributedChips: _.max([result.contributedChips - bet, 0]),
            winningChips: result.winningChips + winningChips
          };
        });
      });
      return calculate(updateSorted);
    }

    const sorted = _.chain(players)
      .groupBy(player => {
        const bestHand = player.getHand().getBestPokerHand();
        return player.isFolded() ? -2 : bestHand && bestHand.getScore() || -1;
      })
      .sortBy((group, score) => { return score * -1; })
      .map(group => {
        return _.chain(group)
          .map((player): PotResult => {
            return {
              player: player,
              contributedChips: player.contributedChips,
              winningChips: 0
            };
          })
          .sortBy(p => { return p.contributedChips; })
          .value();
      })
      .value();
    return _(calculate(sorted)).flatten();
  }
}

export interface PotResult {
  player: Player;
  contributedChips: number;
  winningChips: number;
}
