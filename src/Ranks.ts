/// <reference path="../typings/underscore/underscore.d.ts" />

export default class Ranks {

  private short: string;
  constructor(private full: string, private value: number) {
    this.short = this.full == '10' ? 'T' : this.full.charAt(0);
  }

  getFullName() { return this.full; }

  getShortName() { return this.short; }

  getValue() { return this.value }

  static NUM_RANKS = 13;

  static values: RanksDictionary = {
    DEUCE: new Ranks('2', 2),
    THREE: new Ranks('3', 3),
    FOUR: new Ranks('4', 4),
    FIVE: new Ranks('5', 5),
    SIX: new Ranks('6', 6),
    SEVEN: new Ranks('7', 7),
    EIGHT: new Ranks('8', 8),
    NINE: new Ranks('9', 9),
    TEN: new Ranks('10', 10),
    JACK: new Ranks('Jack', 11),
    QUEEN: new Ranks('Queen', 12),
    KING: new Ranks('King', 13),
    ACE: new Ranks('Ace', 14),
  }
}

export interface RanksDictionary extends _.Dictionary<Ranks> {
  DEUCE: Ranks,
  THREE: Ranks,
  FOUR: Ranks,
  FIVE: Ranks,
  SIX: Ranks,
  SEVEN: Ranks,
  EIGHT: Ranks,
  NINE: Ranks,
  TEN: Ranks,
  JACK: Ranks,
  QUEEN: Ranks,
  KING: Ranks,
  ACE: Ranks
}
