import * as _ from 'underscore';
import Card from './Card';
import PokerHand from './PokerHand';

export default class Hand {

  constructor(private cards: Card[] = []) {}

  addCard(c: Card) {
    this.cards.push(c);
  }

  getCards() { return this.cards; }

  size() { return this.cards.length; }

  getBestPokerHand(): PokerHand { return Hand.findBestPokerHand(this.cards); }

  static findBestPokerHand(cards: Card[]): PokerHand {
    if (cards.length > 5) {
      return _.chain(cards)
        .map((card, index, cards) => { 
          return Hand.findBestPokerHand(_.without(cards, card)); 
        })
        .max(hand => hand.getScore())
        .value();
    } else if (cards.length == 5) {
      return new PokerHand(cards);
    } else {
      return null;
    }
  }

  prettyPrint() { return this.cards.map(c => c.prettyPrint()).join(' | '); }
}
