import Hand from './Hand';
import Card from './Card';

export default class Player {

  hand: Hand = new Hand();
  betAmount: number = 0;
  contributedChips: number = 0;
  folded: boolean = false;

  constructor(public chips: number, public seatIndex: number) {}

  addCard(c: Card) {
    this.hand.addCard(c);
  }

  newHand(hand: Hand = new Hand()) {
    this.hand = hand;
    this.folded = false;
    this.contributedChips = 0;
  }

  getHand() {
    return this.hand;
  }

  contribute(chips: number) {
    const contributedChips = this.chips > chips ? chips : this.chips;
    this.chips -= contributedChips;
    this.contributedChips += contributedChips;
    this.betAmount += contributedChips;
  }

  payBet(chips: number) {
    chips = chips > this.contributedChips ? this.contributedChips : chips;
    this.contributedChips -= chips;
    return chips; 
  }

  fold() {
    this.folded = true;
  }

  isFolded() {
    return this.folded;
  }

  isAlive() {
    return !this.folded && this.chips > 0;
  }

  isAllIn() {
    return !this.folded && this.chips === 0;
  }

}
