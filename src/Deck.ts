import * as _ from 'underscore';

import Card from './Card';
import Suits from './Suits';
import Ranks from './Ranks';

export default class Deck {

  private cards: Card[] = [];
  private curCard: number = 0;

  constructor() {
    _.each(Suits.values, s => {
      _.each(Ranks.values, r => {
        this.cards.push(new Card(r, s));
      });
    });
  }

  dealCard() { return this.cards[this.curCard++]; }

  shuffle() {
    this.curCard = 0;
    this.cards = _(this.cards).sortBy(() => { return Math.random(); });
  }

}