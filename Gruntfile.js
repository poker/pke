var _ = require('underscore');

module.exports = function(grunt) {

  var source;

  source = ['**/*.ts', '!target/**/*.ts', '!node_modules/**/*.ts', '!test/*_.ts'];

  var tsTask = {
    src: source,
    baseDir: '.',
    outDir: 'target/',
    options:  { module: 'commonjs' }
  };

  var tsWatchTask = _.clone(tsTask);
  tsWatchTask.watch = source;

  var tsBuildTask = {
    src: ['src/**/*.ts'],
    baseDir: '.',
    outDir: 'build/',
    options: {
      declaration: true,
      module: 'commonjs'
    }
  };

  grunt.initConfig({
    ts: {
      compile: tsTask,
      build: tsBuildTask,
      watch: tsWatchTask
    },
    tsd: {
      install: {
        options: {
          command: 'reinstall',
          config: './tsd.json'
        }
      }
    },
    clean: {
      dev: ['target/', 'typings/'],
      build: ['build/']
    },
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          clearRequireCache: true
        },
        src: 'target/test/*.js'
      }
    },
    watch: {
      test: {
        files: 'target/**/*.js',
        tasks: ['mochaTest:test'],
        options: {
          spawn: false,
        }
      }
    },
    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      default: {
        tasks: ['ts:watch', 'watch:test']
      }
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['test-dev']);
  grunt.registerTask('test-dev', ['clean:dev', 'tsd:install', 'concurrent:default']);
  grunt.registerTask('travis', ['clean:dev', 'tsd:install', 'ts:compile', 'mochaTest:test']);
  grunt.registerTask('build', ['clean:build', 'tsd:install', 'ts:build']);

  // On watch events, if the changed file is a test file then configure mochaTest to only
  // run the tests from that file. Otherwise run all the tests
  var defaultTestSrc = grunt.config('mochaTest.test.src');
  grunt.event.on('watch', function(action, filepath) {
    grunt.config('mochaTest.test.src', defaultTestSrc);
    if (filepath.match('target/test/')) {
      grunt.config('mochaTest.test.src', filepath);
    }
  });
};
